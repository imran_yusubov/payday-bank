package com.nyse.model.orders;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long externalId;

    private String stockId; //SYMBOL

    private Double maxPrice;

    private String hookURL;

    private Long numberOfShares;

    private String type; //buy, sell

    private OrderStatus status;

    private Date created = new Date();

    private Date updated = new Date();
}
