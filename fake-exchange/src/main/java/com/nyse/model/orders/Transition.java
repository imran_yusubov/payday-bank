package com.nyse.model.orders;

import com.nyse.dto.OrderDto;

public interface Transition {

    String getName();

    OrderStatus getStatus();

    //This should do required pre processing
    void applyProcessing(OrderDto order);
}
