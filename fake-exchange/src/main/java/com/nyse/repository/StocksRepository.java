package com.nyse.repository;

import com.nyse.model.stocks.Stock;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface StocksRepository extends PagingAndSortingRepository<Stock, Long>, JpaSpecificationExecutor<Stock> {

}
