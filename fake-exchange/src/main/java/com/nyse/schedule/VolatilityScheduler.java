package com.nyse.schedule;

import com.nyse.model.orders.Order;
import com.nyse.model.orders.OrderStatus;
import com.nyse.model.stocks.Stock;
import com.nyse.repository.OrderRepository;
import com.nyse.repository.StocksRepository;
import com.nyse.service.TransitionService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class VolatilityScheduler {

    private final StocksRepository stocksRepository;
    private final OrderRepository orderRepository;
    private final TransitionService transitionService;

    @Scheduled(fixedRate = 1000)
    public void createVolatility() {
        long start = System.currentTimeMillis();
        log.info("Updating indexes");
        stocksRepository.findAll().forEach((s) -> calculatePrice(s));
        log.info("Updating indexes completed in {}", System.currentTimeMillis() - start);
    }

    private void calculatePrice(Stock stock) {
        final double random = Math.random();
        double volatility = 0.01;
        double changePercent = 2 * volatility * random;
        if (changePercent > volatility) {
            changePercent -= (2 * volatility);
        }
        double changeAmount = stock.getCurrent() * changePercent;
        double newPrice = stock.getCurrent() + changeAmount;
        save(stock, newPrice);
    }

    private void save(Stock stock, Double newPrice) {
        // log.info("symbol: {} current: {} new: {}", stock.getSymbol(), stock.getCurrent(), newPrice);
        stock.setCurrent(newPrice);
        stocksRepository.save(stock);
    }

    private void updateOrders(Stock stock) {
        //Update buy orders
        List<Order> buy = orderRepository
                .findByStockIdAndStatusAndMaxPriceLessThanAndType(stock.getSymbol(), OrderStatus.NEW,
                        stock.getCurrent(), "buy");
        updateOrders(buy);

        //Update sell orders
        final List<Order> sell = orderRepository
                .findByStockIdAndStatusAndMaxPriceGreaterThanAndType(stock.getSymbol(), OrderStatus.NEW,
                        stock.getCurrent(), "sell");
        updateOrders(sell);
    }

    private void updateOrders(List<Order> orders) {
        orders.stream()
                .forEach(order -> {
                  transitionService.transitionOrder(order.getId(),"filled");
                });
    }

}
