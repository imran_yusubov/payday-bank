package com.nyse.service;

import com.nyse.dto.ClientOrderDto;
import com.nyse.dto.ClientOrderStatusDto;
import com.nyse.dto.OrderDto;
import com.nyse.events.OrderStatusChangeEvent;
import com.nyse.model.orders.Order;
import com.nyse.model.orders.OrderStatus;
import com.nyse.repository.OrderRepository;
import com.payday.bank.common.dto.GenericSearchDto;
import com.payday.bank.common.repository.search.SearchCriteria;
import com.payday.bank.common.repository.search.SearchOperation;
import com.payday.bank.common.repository.search.SearchSpecification;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class OrderService {

    private final OrderRepository orderRepository;
    private final TransitionService transitionService;
    private final ModelMapper modelMapper;

    @Transactional
    public ClientOrderStatusDto createOrder(Long clientId, ClientOrderDto clientOrderDto) {
        Order order = new Order();
        order.setExternalId(clientOrderDto.getId());
        order.setHookURL(clientOrderDto.getHookURL());
        order.setMaxPrice(clientOrderDto.getMaxPrice());
        order.setNumberOfShares(clientOrderDto.getNumberOfShares());
        order.setStockId(clientOrderDto.getStockId());
        order.setType(order.getType());
        order.setStatus(OrderStatus.NEW);
        orderRepository.save(order);

        ClientOrderStatusDto orderStatus = new ClientOrderStatusDto();
        orderStatus.setExternalId(clientOrderDto.getId());
        orderStatus.setId(order.getId());
        orderStatus.setStatus(order.getStatus().name());
        return orderStatus;
    }


    public String checkStatus(Long clientId, Long orderId) {
        return orderRepository.findById(orderId).orElseThrow(() -> new RuntimeException("Order not found"))
                .getStatus().name();
    }

    @Transactional
    public void updateOrder(Long orderId, String transition) {
        transitionService.transitionOrder(orderId, transition);
    }

    @EventListener
    void handleUserRemovedEvent(OrderStatusChangeEvent event) {
        System.out.println("Order status changed: " + event);

    }

    public Page<OrderDto> findAll(GenericSearchDto filter, Pageable pageable) {
        return orderRepository.findAll(new SearchSpecification<>(filter.getCriteria()), pageable)
                .map(p -> modelMapper.map(p, OrderDto.class));
    }
}
