package com.nyse.service;

import com.nyse.dto.OrderDto;
import com.nyse.events.OrderStatusChangeEvent;
import com.nyse.model.orders.Order;
import com.nyse.model.orders.OrderStatus;
import com.nyse.model.orders.Transition;
import com.nyse.repository.OrderRepository;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.modelmapper.ModelMapper;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TransitionService {
    //@Todo : duplicate code with account-manager, need to refactor and create a common transition handling mechanism

    private Map<String, Transition> transitionsMap;
    private final ModelMapper mapper;
    private final ApplicationEventPublisher applicationEventPublisher;
    private final OrderRepository orderRepository;

    public TransitionService(List<Transition> transitions, ModelMapper mapper,
            ApplicationEventPublisher applicationEventPublisher, OrderRepository orderRepository) {
        this.mapper = mapper;
        this.applicationEventPublisher = applicationEventPublisher;
        this.orderRepository = orderRepository;
        initTransitions(transitions);
    }

    public void initTransitions(List<Transition> transitions) {
        Map<String, Transition> transitionHashMap = new HashMap<>();
        for (Transition transition : transitions) {
            if (transitionHashMap.containsKey(transition.getName())) {
                throw new IllegalStateException("Duplicate transition" + transition.getName());
            }
            transitionHashMap.put(transition.getName(), transition);
        }
        transitionsMap = Collections.unmodifiableMap(transitionHashMap);
    }

    public List<String> getAllowedTransitions(Long id) {
        Order profile = orderRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Unknown order: " + id));
        return profile.getStatus().getTransitions();
    }

    @Transactional
    public OrderDto transitionOrder(Long id, String transitionName) {
        Transition transition = transitionsMap.get(transitionName);
        if (transition == null) {
            throw new IllegalArgumentException("Unknown transition: " + transitionName);
        }
        return orderRepository.findById(id)
                .map(order -> {
                    checkAllowed(transition, order.getStatus());
                    transition.applyProcessing(mapper.map(order, OrderDto.class));
                    order.setUpdated(new Date());
                    return updateStatus(order, transition.getStatus());
                })
                .map(u -> mapper.map(u, OrderDto.class))
                .orElseThrow(() -> new IllegalArgumentException("Unknown order: " + id));
    }

    private void checkAllowed(Transition transition, OrderStatus status) {
        Set<OrderStatus> allowedSourceStatuses = Stream.of(OrderStatus.values())
                .filter(s -> s.getTransitions().contains(transition.getName()))
                .collect(Collectors.toSet());
        if (!allowedSourceStatuses.contains(status)) {
            throw new RuntimeException("The transition from the " + status.name() + " status to the "
                    + transition.getStatus().name() + " status is not allowed!");
        }
    }

    private Order updateStatus(Order order, OrderStatus updatedStatus) {
        OrderStatus existingStatus = order.getStatus();
        order.setStatus(updatedStatus);
        Order updated = orderRepository.save(order);

        var event = new OrderStatusChangeEvent(this, existingStatus.name(), mapper.map(updated, OrderDto.class));
        applicationEventPublisher.publishEvent(event);
        return updated;
    }

}
