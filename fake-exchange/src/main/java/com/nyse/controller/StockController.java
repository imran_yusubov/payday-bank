package com.nyse.controller;

import com.nyse.dto.ClientOrderDto;
import com.nyse.dto.ClientOrderStatusDto;
import com.nyse.dto.OrderDto;
import com.nyse.dto.StockDto;
import com.nyse.service.OrderService;
import com.nyse.service.StocksService;
import com.nyse.service.TransitionService;
import com.payday.bank.common.dto.GenericSearchDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("stocks/{clientId}")
@RequiredArgsConstructor
public class StockController {

    private final OrderService orderService;
    private final StocksService stocksService;
    private final TransitionService transitionService;

    @PostMapping
    public ClientOrderStatusDto placeOrder(@PathVariable Long clientId, @RequestBody ClientOrderDto clientOrderDto) {
        log.trace("Creating order:" + clientOrderDto);
        return orderService.createOrder(clientId, clientOrderDto);
    }

    @GetMapping
    public String checkStatus(@PathVariable Long clientId, @RequestParam Long id) {
        return orderService.checkStatus(clientId, id);
    }

    @PostMapping("/transition")
    public OrderDto updateStatus(@PathVariable Long clientId, @RequestParam Long id, @RequestParam String transition) {
        return transitionService.transitionOrder(id, transition);
    }

    @PostMapping("/orders/search")
    public Page<OrderDto> searchAllOrders(@RequestBody GenericSearchDto filter, Pageable pageable) {
        return orderService.findAll(filter, pageable);
    }

    @PostMapping("/search")
    public Page<StockDto> searchAllStocks(@RequestBody GenericSearchDto filter, Pageable pageable) {
        return stocksService.findAll(filter, pageable);
    }
}
