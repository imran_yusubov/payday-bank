package com.nyse.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Builder
@RequiredArgsConstructor
@AllArgsConstructor
public class ClientOrderStatusDto {

    private Long id;
    private Long externalId;
    private String status;
}
