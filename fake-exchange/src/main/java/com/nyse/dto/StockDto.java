package com.nyse.dto;

import lombok.Data;

@Data
public class StockDto {

    private Long id;
    private Double open;
    private Double current;
    private String symbol;
    private String description;
    private String index;
}
