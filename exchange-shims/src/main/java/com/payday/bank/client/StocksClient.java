package com.payday.bank.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
//@ToDo: this class does a lot, need to split
public class StocksClient {

    private KafkaTemplate<String, OrderStatusDto> orderStatusTemplate;
    private String ordersStatusTopic;
    private final String stockApi;
    private final String hookUrl;
    private final RestTemplate restTemplate;

    public StocksClient(
            @Value("${exchange.stocks_api}") String stockApi,
            KafkaTemplate<String, OrderStatusDto> orderStatusTemplate,
            @Value("${kafka.topics.orders.status}") String ordersStatusTopic,
            @Value("${hookUrl}") String hookUrl, RestTemplate restTemplate) {
        this.orderStatusTemplate = orderStatusTemplate;
        this.ordersStatusTopic = ordersStatusTopic;
        this.stockApi = stockApi;
        this.hookUrl = hookUrl;
        this.restTemplate = restTemplate;
    }

    //@Todo: implement proper retry mechanism on failures
    @KafkaListener(containerFactory = "ordersKafkaListenerContainerFactory", topics = "${kafka.topics.orders.create}", groupId = "${kafka.consumer.groupId.orders}")
    public void listen(CreateOrderDto createOrderDto) {
        log.trace("Received order:" + createOrderDto);
        placeOrder(createOrderDto);
    }

    public void updateOrderStatus(OrderStatusDto orderStatusDto, String status) {
        log.trace("Updating status: " + orderStatusDto);
        orderStatusDto.setStatus(status);
        long exId = orderStatusDto.getId();
        orderStatusDto.setId(orderStatusDto.getExternalId());
        orderStatusDto.setExternalId(exId);
        orderStatusTemplate.send(ordersStatusTopic, orderStatusDto);
    }

    private void placeOrder(CreateOrderDto createOrderDto) {
        log.trace("Creating order:" + createOrderDto);
        createOrderDto.setHookURL(hookUrl);
        ResponseEntity<OrderStatusDto> response
                = restTemplate.postForEntity(stockApi, createOrderDto, OrderStatusDto.class);
        if (response.getStatusCode().is2xxSuccessful()) {
            updateOrderStatus(response.getBody(), "submitted");
        }
    }

}
