package com.payday.bank.client;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateOrderDto {

    private Long id;
    private Double maxPrice;
    private String symbol;
    private Long numberOfShares;
    private String type;
    private String user;
    private String hookURL;
}
