package com.payday.bank.client;

import lombok.Data;

@Data
public class OrderStatusDto {

    private Long id;
    private Long externalId;
    private String status;
}
