package com.payday.bank.controller;

import com.payday.bank.client.OrderStatusDto;
import com.payday.bank.client.StocksClient;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("orders")
@RequiredArgsConstructor
public class OrderStatusResponseController {

    private final StocksClient stocksClient;

    @PostMapping //@ToDo: should use something , to make it idempotent.& need to implement auth token
    public void acceptResponseStatus(@RequestBody @Valid OrderStatusDto statusDto) {
        log.trace("Status received:" + statusDto);
        //@ToDo: need to extract it into  service and handle status mapping dynamically
        if (statusDto.getStatus().equalsIgnoreCase("filled")) {
            stocksClient.updateOrderStatus(statusDto, "filled");
        } else {
            stocksClient.updateOrderStatus(statusDto, "failed");
        }
    }
}
