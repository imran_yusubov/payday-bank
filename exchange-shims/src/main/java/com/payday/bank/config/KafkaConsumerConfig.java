package com.payday.bank.config;

import com.payday.bank.client.CreateOrderDto;
import com.payday.bank.client.OrderStatusDto;
import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

@EnableKafka
@Configuration
public class KafkaConsumerConfig {

    @Value("${kafka.bootstrapAddress}")
    private String bootstrapAddress;

    @Value("${kafka.consumer.groupId.orders}")
    private String kafkaOrdersConsumerGroupId;

    @Value("${kafka.consumer.groupId.orders_statuses}")
    private String kafkaOrderStatusesConsumerGroupId;

    public ConsumerFactory<String, CreateOrderDto> orderConsumerFactory() {
        Map<String, Object> props = new HashMap<>();
        doCommonConfig(props);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, kafkaOrdersConsumerGroupId);
        return new DefaultKafkaConsumerFactory<>(props, new StringDeserializer(),
                new JsonDeserializer<>(CreateOrderDto.class));
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, CreateOrderDto> ordersKafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, CreateOrderDto> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(orderConsumerFactory());
        return factory;
    }

    public ConsumerFactory<String, OrderStatusDto> orderStatusConsumerFactory() {
        Map<String, Object> props = new HashMap<>();
        doCommonConfig(props);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, kafkaOrdersConsumerGroupId);
        return new DefaultKafkaConsumerFactory<>(props, new StringDeserializer(),
                new JsonDeserializer<>(OrderStatusDto.class));
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, OrderStatusDto> ordersStatusKafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, OrderStatusDto> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(orderStatusConsumerFactory());
        return factory;
    }

    private void doCommonConfig(Map<String, Object> configProps) {
        configProps.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
    }
}
