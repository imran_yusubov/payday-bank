package com.payday.bank.common.repository;


import java.util.function.Function;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;
import org.springframework.util.StringUtils;

/**
 * Predicate builder to apply like predicate to non empty field
 */
public class LikePredicateBuilder<T,E> extends AbstractPredicateBuilder<String, T, E> {

    private final SingularAttribute<E, String> field;

    public LikePredicateBuilder(Function<T, String> retriever,
                                SingularAttribute<E, String> field) {
        super(retriever);
        this.field = field;
    }

    @Override
    protected boolean isBad(String value) {
        return StringUtils.isEmpty(value);
    }

    @Override
    protected Predicate provide(Root<E> root, CriteriaBuilder bldr, String value) {
        return bldr.like(root.get(field), "%" + value + "%");
    }
}
