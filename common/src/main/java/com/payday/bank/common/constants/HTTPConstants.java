package com.payday.bank.common.constants;

public class HTTPConstants {

    public static String APPLICATION_JSON = "application/json";

    public static String ACCEPT_HEADER = "accept";
    public static String CONTENT_TYPE = "Content-Type";

    public static final String AUTH_HEADER = "Authorization";

    public static final String BASIC_AUTH_HEADER = "Basic";
    public static final String BEARER_AUTH_HEADER = "Bearer";


    private HTTPConstants() {
        throw new UnsupportedOperationException();
    }
}
