package com.payday.bank.notifications;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

@Configuration
public class EmailSender {

    private final JavaMailSender emailSender;

    private final String sourceEmail;

    public EmailSender(JavaMailSender emailSender, @Value("${from.email}")
            String sourceEmail) {
        this.emailSender = emailSender;
        this.sourceEmail = sourceEmail;
    }

    public void sendSimpleMessage(SimpleMailMessage mailMessage) {
        emailSender.send(mailMessage);
    }
}
