package com.payday.bank;

import com.payday.bank.common.config.LoggingTcpConfiguration;
import com.payday.bank.common.dto.Email;
import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.core.KafkaTemplate;

@SpringBootApplication
@Import({LoggingTcpConfiguration.class})
public class NotificationsManagerApplication implements CommandLineRunner {

    @Autowired
    private KafkaTemplate<String, Email> kafkaTemplate;

    @Value("${kafka.topics.emails}")
    private String emailTopic;

    public static void main(String[] args) {
        SpringApplication.run(NotificationsManagerApplication.class, args);
    }

    @Override
    public void run(String... args) {
        Email email = Email.builder()
                .from("payday.bank.exchange@gmail.com")
                .to("imran_yusubov@yahoo.com")
                .subject("Account Notifications")
                .text("Your account has been created :" + LocalDateTime.now())
                .build();
        kafkaTemplate.send(emailTopic, email);
    }

}
