package com.payday.bank.consumers;

import com.payday.bank.common.dto.Email;
import com.payday.bank.notifications.EmailSender;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class EmailConsumer {

    @Autowired
    private EmailSender emailSender;

    @KafkaListener(containerFactory = "emailKafkaListenerContainerFactory", topics = "${kafka.topics.emails}", groupId = "${kafka.consumer.groupId}")
    public void listen(Email message) {
        log.trace("Received message:" + message);
        emailSender.sendSimpleMessage(mapToMessage(message));
    }

    private SimpleMailMessage mapToMessage(Email email) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom(email.getFrom());
        simpleMailMessage.setTo(email.getTo());
        simpleMailMessage.setSubject(email.getSubject());
        simpleMailMessage.setText(email.getText());
        return simpleMailMessage;
    }
}
