package com.payday.bank.model.orders.transitions;

import com.payday.bank.dto.OrderDto;
import com.payday.bank.model.orders.OrderStatus;
import com.payday.bank.model.orders.OrderTransition;
import org.springframework.stereotype.Service;

@Service
public class Filled implements OrderTransition {

    public static final String NAME = "filled";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public OrderStatus getStatus() {
        return OrderStatus.FILLED;
    }

    @Override
    public void applyProcessing(OrderDto order) {
        //Do processing here
    }
}
