package com.payday.bank.model.user;

public enum UserRole {
    ADMIN,
    CUSTOMER
}
