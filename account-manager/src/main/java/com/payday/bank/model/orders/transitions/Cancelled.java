package com.payday.bank.model.orders.transitions;

import com.payday.bank.dto.OrderDto;
import com.payday.bank.model.orders.OrderStatus;
import com.payday.bank.model.orders.OrderTransition;
import org.springframework.stereotype.Service;

@Service
public class Cancelled implements OrderTransition {

    public static final String NAME = "cancelled";

    @Override
    public String getName() {
        return null;
    }

    @Override
    public OrderStatus getStatus() {
        return OrderStatus.CANCELLED;
    }

    @Override
    public void applyProcessing(OrderDto order) {
        //Do processing here
    }
}
