package com.payday.bank.model.orders;

import com.payday.bank.dto.OrderDto;

public interface OrderTransition {

    String getName();

    OrderStatus getStatus();

    //This should do required pre processing
    void applyProcessing(OrderDto order);
}
