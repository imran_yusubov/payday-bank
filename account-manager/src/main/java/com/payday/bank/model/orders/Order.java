package com.payday.bank.model.orders;

import com.payday.bank.model.Account;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = Order.TABLE_NAME, indexes = {
        @Index(columnList = Order.STATUS_COLUMN, name = Order.STATUS_INDEX, unique = true)
})
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Order {

    public static final String TABLE_NAME = "ORDERS";
    public static final String INDEX = "_index";
    public static final String NAME_COLUMN = "name";
    public static final String NAME_INDEX = NAME_COLUMN + INDEX;
    public static final String STATUS_COLUMN = "status";
    public static final String STATUS_INDEX = STATUS_COLUMN + INDEX;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account account;

    private Double maxPrice;

    private String symbol;

    private Long numberOfShares;

    private String type;

    private OrderStatus status;

    private Date created = new Date();

    private Date updated = new Date();
}
