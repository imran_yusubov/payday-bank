package com.payday.bank.model.orders;

import com.payday.bank.model.orders.transitions.Cancelled;
import com.payday.bank.model.orders.transitions.Cancelling;
import com.payday.bank.model.orders.transitions.Failed;
import com.payday.bank.model.orders.transitions.Filled;
import com.payday.bank.model.orders.transitions.Pending;
import com.payday.bank.model.orders.transitions.Submitted;
import java.util.Arrays;
import java.util.List;

public enum OrderStatus {

    NEW(Pending.NAME, Cancelling.NAME), //Order created locally
    PENDING(Submitted.NAME, Cancelling.NAME, Failed.NAME), //Order created locally && submitted to ex system
    SUBMITTED(Filled.NAME, Failed.NAME,
            Cancelling.NAME), //Order sent to external system and a successfully acknowledged
    FILLED(), //Given price is reached and external system sent a successful buy/sell response with web hock
    FAILED(), // Failed to send the request to external system for some reason or other issues happened
    CANCELLING(Cancelled.NAME), //User tries to cancel the order, and a message is sent to queue
    //If not able to send the order to external system for some max number of attempts, revert back to submitted
    CANCELLED();//External system removes the order and notifies with web hook

    private final List<String> transitions;

    OrderStatus(String... transitions) {
        this.transitions = Arrays.asList(transitions);
    }

    public List<String> getTransitions() {
        return transitions;
    }
}
