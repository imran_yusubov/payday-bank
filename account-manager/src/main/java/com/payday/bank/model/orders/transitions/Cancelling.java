package com.payday.bank.model.orders.transitions;

import com.payday.bank.dto.OrderDto;
import com.payday.bank.model.orders.OrderStatus;
import com.payday.bank.model.orders.OrderTransition;
import org.springframework.stereotype.Service;

@Service
public class Cancelling implements OrderTransition {

    public static final String NAME = "cancelling";

    @Override
    public String getName() {
        return Cancelling.NAME;
    }

    @Override
    public OrderStatus getStatus() {
        return OrderStatus.CANCELLING;
    }

    @Override
    public void applyProcessing(OrderDto order) {
        //Do processing here
    }
}
