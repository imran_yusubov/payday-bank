package com.payday.bank.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = StockSymbols.TABLE_NAME, indexes = {
        @Index(columnList = StockSymbols.NAME_COLUMN, name = StockSymbols.NAME_INDEX, unique = true),
        @Index(columnList = StockSymbols.SYMBOL_COLUMN, name = StockSymbols.SYMBOL_INDEX, unique = true)
})
@Data
public class StockSymbols {

    public static final String TABLE_NAME = "STOCK_SYMBOLS";
    public static final String INDEX = "_index";
    public static final String NAME_COLUMN = "name";
    public static final String NAME_INDEX = NAME_COLUMN + INDEX;
    public static final String SYMBOL_COLUMN = "name";
    public static final String SYMBOL_INDEX = SYMBOL_COLUMN + INDEX;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String symbol;

    private String name;

    private String index;
}
