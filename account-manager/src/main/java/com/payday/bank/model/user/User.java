package com.payday.bank.model.user;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = User.TABLE_NAME, indexes = {
        @Index(columnList = User.USER_NAME_COLUMN, name = User.USER_NAME_INDEX, unique = true),
})
@Data
public class User {

    public static final String TABLE_NAME = "USERS";
    public static final String INDEX = "_index";
    public static final String USER_NAME_COLUMN = "user_name";
    public static final String USER_NAME_INDEX = USER_NAME_COLUMN + INDEX;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = USER_NAME_COLUMN)
    private String userName;
    private String fullName;
    private String email;
    private UserRole role;

    private Date created;
}
