package com.payday.bank.model.orders.transitions;

import com.payday.bank.dto.OrderDto;
import com.payday.bank.model.orders.OrderStatus;
import com.payday.bank.model.orders.OrderTransition;
import org.springframework.stereotype.Service;

@Service
public class Submitted implements OrderTransition {

    public static final String NAME = "submitted";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public OrderStatus getStatus() {
        return OrderStatus.SUBMITTED;
    }

    @Override
    public void applyProcessing(OrderDto order) {
        //Do processing here
    }
}

