package com.payday.bank.model.orders.transitions;

import com.payday.bank.client.CreateOrderDto;
import com.payday.bank.dto.OrderDto;
import com.payday.bank.model.orders.OrderStatus;
import com.payday.bank.model.orders.OrderTransition;
import com.payday.bank.service.AccountService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class Pending implements OrderTransition {

    public static final String NAME = "pending";

    private final ModelMapper modelMapper;
    private final KafkaTemplate<String, CreateOrderDto> createOrderTemplate;
    private final String createOrdersTopic;
    private final AccountService accountService;

    public Pending(
            ModelMapper modelMapper, KafkaTemplate<String, CreateOrderDto> createOrderTemplate,
            @Value("${kafka.topics.orders.create}") String createOrdersTopic,
            AccountService accountService) {
        this.modelMapper = modelMapper;
        this.createOrderTemplate = createOrderTemplate;
        this.createOrdersTopic = createOrdersTopic;
        this.accountService = accountService;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public OrderStatus getStatus() {
        return OrderStatus.PENDING;
    }

    @Override
    public void applyProcessing(OrderDto orderDto) {
        final CreateOrderDto map = modelMapper.map(orderDto, CreateOrderDto.class);
        final String userName = accountService.findAccountById(orderDto.getAccount().getId()).getUser().getUserName();
        map.setUser(userName);
        createOrderTemplate.send(createOrdersTopic, map);
    }
}
