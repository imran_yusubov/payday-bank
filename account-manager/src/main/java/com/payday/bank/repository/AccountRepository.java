package com.payday.bank.repository;

import com.payday.bank.model.Account;
import com.payday.bank.model.user.User;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<Account, Long> {

    Optional<Account> findByUser(User user);
}
