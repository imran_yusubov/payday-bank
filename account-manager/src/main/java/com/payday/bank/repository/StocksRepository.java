package com.payday.bank.repository;

import com.payday.bank.model.StockSymbols;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface StocksRepository extends PagingAndSortingRepository<StockSymbols, Long>,
        JpaSpecificationExecutor<StockSymbols> {

}
