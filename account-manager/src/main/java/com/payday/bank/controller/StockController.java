package com.payday.bank.controller;

import com.payday.bank.common.dto.GenericSearchDto;
import com.payday.bank.dto.CreateOrderDto;
import com.payday.bank.dto.OrderDto;
import com.payday.bank.dto.StockDto;
import com.payday.bank.model.orders.transitions.Pending;
import com.payday.bank.service.StockService;
import com.payday.bank.service.TransitionService;
import java.security.Principal;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("stocks")
public class StockController {

    private final StockService stockService;
    private final TransitionService transitionService;

    @GetMapping
    public String getMapping() {
        final Sort id = Sort.by(Order.asc("id"));
        PageRequest pageRequest = PageRequest.of(1, 2000, id);

        return "Stocks";
    }

    @PostMapping("/symbols/search")
    public Page<StockDto> searchAll(@RequestBody GenericSearchDto filter, Pageable pageable) {

        return stockService.findAll(filter, pageable);
    }

    @PostMapping("/orders")
    public OrderDto placeOrder(@Valid @RequestBody CreateOrderDto createOrderDto, Principal user) {
        OrderDto orderDto=stockService.placeOrder(createOrderDto,user.getName());
        transitionService.transitionOrder(orderDto.getId(), Pending.NAME); //@ToDo: don't liked to call it from here, if app crashes it will always be in NEW status. Not good user experience
        return orderDto;
    }

    @PostMapping("/orders/search")
    public Page<OrderDto> searchAll(@RequestBody GenericSearchDto filter, Pageable pageable, Principal user) {

        return stockService.findAllOrders(filter, pageable,user.getName());
    }

}
