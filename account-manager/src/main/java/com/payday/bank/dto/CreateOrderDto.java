package com.payday.bank.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.Data;

@Data
public class CreateOrderDto {

    @NotNull
    @Positive
    private Double maxPrice;

    @NotNull
    @NotEmpty
    private String symbol;

    @NotNull
    @Positive
    private Long numberOfShares;

    @NotNull
    @NotEmpty
    private String type;

}
