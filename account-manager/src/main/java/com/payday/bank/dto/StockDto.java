package com.payday.bank.dto;

import lombok.Data;

@Data
public class StockDto {

    private String symbol;

    private String name;

    private String index;
}
