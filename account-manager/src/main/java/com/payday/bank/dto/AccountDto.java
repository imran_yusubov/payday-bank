package com.payday.bank.dto;

import lombok.Data;

@Data
public class AccountDto {

    private Long id;

    private UserDto user;

    private Double cashBalance;

    private Double totalBalance;
}
