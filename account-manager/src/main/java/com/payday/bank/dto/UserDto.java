package com.payday.bank.dto;

import com.payday.bank.model.user.UserRole;
import java.util.Date;
import lombok.Data;

@Data
public class UserDto {

    private long id;

    private String userName;
    private String password;
    private UserRole role;

    private Date created;
}
