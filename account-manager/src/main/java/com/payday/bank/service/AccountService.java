package com.payday.bank.service;

import com.payday.bank.common.dto.Email;
import com.payday.bank.dto.AccountDto;
import com.payday.bank.dto.UserDto;
import com.payday.bank.exceptions.AlreadyExistException;
import com.payday.bank.exceptions.NotFoundException;
import com.payday.bank.model.Account;
import com.payday.bank.model.user.User;
import com.payday.bank.repository.AccountRepository;
import com.payday.bank.repository.UserRepository;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
public class AccountService {

    private final UserRepository userRepository;
    private final AccountRepository accountRepository;
    private final ModelMapper mapper;
    private final String emailTopic;
    private final KafkaTemplate<String, Email> kafkaTemplate;

    public AccountService(UserRepository userRepository, AccountRepository accountRepository,
            ModelMapper mapper, @Value("${kafka.topics.emails}") String emailTopic,
            KafkaTemplate<String, Email> kafkaTemplate) {
        this.userRepository = userRepository;
        this.accountRepository = accountRepository;
        this.mapper = mapper;
        this.emailTopic = emailTopic;
        this.kafkaTemplate = kafkaTemplate;
    }

    /**
     * Creates a new account for given user
     *
     * @param userName : user uid
     */
    @Transactional
    public AccountDto createAccount(String userName) {
        log.trace("Creating new account for user {}", userName);
        final User user = findUser(userName);
        Account account = Account
                .builder()
                .cashBalance(0.0)
                .totalBalance(0.0)
                .user(user)
                .build();
        if (findByUser(user).isPresent()) {
            throw new AlreadyExistException("An account for the user is already exist: " + userName);
        }
        account.setUser(findUser(userName));
        accountRepository.save(account);
        kafkaTemplate.send(emailTopic, constructEmail(userName));
        return mapper.map(account, AccountDto.class);
    }

    /**
     * Retrieves the account of the given user
     *
     * @param id: account uid
     * @return : the account dto
     */
    @Transactional
    public AccountDto findAccountById(Long id) {
        log.trace("Find account by id: "+id);
        final Account account = accountRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Account not found"));
        final AccountDto accountDto = mapper.map(account, AccountDto.class);
        accountDto.setUser(mapper.map(account.getUser(), UserDto.class));
        return accountDto;
    }

    /**
     * Retrieves the account of the given user
     *
     * @param userName: user uid
     * @return : the account dto
     */
    @Transactional
    public AccountDto findAccountByUserName(String userName) {
        return findByUser(userName)
                .map((account) -> {
                    AccountDto accountDto = mapper.map(account, AccountDto.class);
                    accountDto.setUser(mapper.map(account.getUser(), UserDto.class));
                    return accountDto;
                })
                .orElseThrow(() -> accountNotFount(userName));
    }

    /**
     * Deposits cash to given user account
     *
     * @param amount : the amount to be deposited, positive
     * @param userName : the account holder user name
     */
    @Transactional
    public AccountDto deposit(Long amount, String userName) {
        if (amount < 0) {
            throw new IllegalArgumentException("The amount must be greater than 0");
        }
        final Account account = findByUser(userName).orElseThrow(() -> accountNotFount(userName));
        account.setCashBalance(account.getCashBalance() + amount);
        return mapper.map(account, AccountDto.class);
    }

    private User findUser(String userName) {
        return userRepository.findByUserName(userName)
                .orElseThrow(() -> new NotFoundException("User not found:" + userName));
    }

    private Optional<Account> findByUser(String userName) {
        return findByUser(findUser(userName));
    }

    private Optional<Account> findByUser(User user) {
        return accountRepository.findByUser(user);
    }

    private NotFoundException accountNotFount(String userName) {
        return new NotFoundException("Account not found for user: " + userName);
    }

    private Email constructEmail(String userName) {
        return Email.builder()
                .from("accounts@payday.com")
                .to("imran_yusubov@yahoo.com")
                .subject("Account Created")
                .text("Dear " + userName + ", your PayDay account is created. Happy Tradings!!!")
                .build();
    }
}
