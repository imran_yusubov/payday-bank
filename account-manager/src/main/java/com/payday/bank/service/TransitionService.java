package com.payday.bank.service;

import com.payday.bank.dto.OrderDto;
import com.payday.bank.events.OrderStatusChangeEvent;
import com.payday.bank.model.orders.Order;
import com.payday.bank.model.orders.OrderStatus;
import com.payday.bank.model.orders.OrderTransition;
import com.payday.bank.repository.OrderRepository;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.modelmapper.ModelMapper;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TransitionService {

    private Map<String, OrderTransition> transitionsMap;
    private final ModelMapper mapper;
    private final ApplicationEventPublisher applicationEventPublisher;
    private final OrderRepository orderRepository;

    public TransitionService(List<OrderTransition> orderTransitions, ModelMapper mapper,
            ApplicationEventPublisher applicationEventPublisher, OrderRepository orderRepository) {
        this.mapper = mapper;
        this.applicationEventPublisher = applicationEventPublisher;
        this.orderRepository = orderRepository;
        initTransitions(orderTransitions);
    }

    public void initTransitions(List<OrderTransition> orderTransitions) {
        Map<String, OrderTransition> transitionHashMap = new HashMap<>();
        for (OrderTransition orderTransition : orderTransitions) {
            if (transitionHashMap.containsKey(orderTransition.getName())) {
                throw new IllegalStateException("Duplicate transition" + orderTransition.getName());
            }
            transitionHashMap.put(orderTransition.getName(), orderTransition);
        }
        transitionsMap = Collections.unmodifiableMap(transitionHashMap);
    }

    public List<String> getAllowedTransitions(Long id) {
        Order order = orderRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Unknown order: " + id));
        return order.getStatus().getTransitions();
    }

    @Transactional
    public OrderDto transitionOrder(Long id, String transitionName) {
        OrderTransition orderTransition = transitionsMap.get(transitionName);
        if (orderTransition == null) {
            throw new IllegalArgumentException("Unknown transition: " + transitionName);
        }
        return orderRepository.findById(id)
                .map(order -> {
                    checkAllowed(orderTransition, order.getStatus());
                    orderTransition.applyProcessing(mapper.map(order, OrderDto.class));
                    order.setUpdated(new Date());
                    return updateStatus(order, orderTransition.getStatus());
                })
                .map(u -> mapper.map(u, OrderDto.class))
                .orElseThrow(() -> new IllegalArgumentException("Unknown order: " + id));
    }

    private void checkAllowed(OrderTransition orderTransition, OrderStatus status) {
        Set<OrderStatus> allowedSourceStatuses = Stream.of(OrderStatus.values())
                .filter(s -> s.getTransitions().contains(orderTransition.getName()))
                .collect(Collectors.toSet());
        if (!allowedSourceStatuses.contains(status)) {
            throw new RuntimeException("The transition from the " + status.name() + " status to the "
                    + orderTransition.getStatus().name() + " status is not allowed!");
        }
    }

    private Order updateStatus(Order order, OrderStatus updatedStatus) {
        OrderStatus existingStatus = order.getStatus();
        order.setStatus(updatedStatus);
        Order updated = orderRepository.save(order);

        var event = new OrderStatusChangeEvent(this, existingStatus.name(), mapper.map(updated, OrderDto.class));
        applicationEventPublisher.publishEvent(event);
        return updated;
    }

}
