package com.payday.bank.service;

import com.payday.bank.client.OrderStatusDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class OrderStatusConsumer {

    private KafkaTemplate<String, OrderStatusDto> orderStatusTemplate;
    private String ordersStatusTopic;
    private final String stockApi;
    private final TransitionService transitionService;

    public OrderStatusConsumer(
            @Value("${exchange.stocks_api}") String stockApi,
            KafkaTemplate<String, OrderStatusDto> orderStatusTemplate,
            @Value("${kafka.topics.orders.status}") String ordersStatusTopic,
            TransitionService transitionService) {
        this.orderStatusTemplate = orderStatusTemplate;
        this.ordersStatusTopic = ordersStatusTopic;
        this.stockApi = stockApi;
        this.transitionService = transitionService;
    }

    @KafkaListener(containerFactory = "ordersStatusKafkaListenerContainerFactory", topics = "${kafka.topics.orders.status}", groupId = "${kafka.consumer.groupId.orders_statuses}")
    public void listen(OrderStatusDto orderStatusDto) {
        log.trace("Received message:" + orderStatusDto);
        transitionService.transitionOrder(orderStatusDto.getId(), orderStatusDto.getStatus());
    }


}
