package com.payday.bank.service;

import com.payday.bank.common.dto.GenericSearchDto;
import com.payday.bank.common.repository.search.SearchCriteria;
import com.payday.bank.common.repository.search.SearchOperation;
import com.payday.bank.common.repository.search.SearchSpecification;
import com.payday.bank.dto.AccountDto;
import com.payday.bank.dto.CreateOrderDto;
import com.payday.bank.dto.OrderDto;
import com.payday.bank.dto.StockDto;
import com.payday.bank.exceptions.InvalidStateException;
import com.payday.bank.exceptions.NotFoundException;
import com.payday.bank.model.Account;
import com.payday.bank.model.orders.Order;
import com.payday.bank.model.orders.OrderStatus;
import com.payday.bank.model.orders.transitions.Pending;
import com.payday.bank.repository.AccountRepository;
import com.payday.bank.repository.OrderRepository;
import com.payday.bank.repository.StocksRepository;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class StockService {

    private final StocksRepository stocksRepository;
    private final ModelMapper mapper;
    private final AccountService accountService;
    private final AccountRepository accountRepository;
    private final OrderRepository orderRepository;
    private final TransitionService transitionService;
    private final ApplicationEventPublisher applicationEventPublisher;


    @Transactional
    public Page<StockDto> findAll(GenericSearchDto filter, Pageable pageable) {
        return stocksRepository.findAll(new SearchSpecification<>(filter.getCriteria()), pageable)
                .map(p -> mapper.map(p, StockDto.class));
    }

    @Transactional
    public OrderDto placeOrder(CreateOrderDto createOrderDto, String name) {
        final AccountDto accountDto = accountService.findAccountByUserName(name);
        final Optional<Account> byId = accountRepository.findById(accountDto.getId());
        if (byId.isPresent()) {
            final Order order = placeOrder(createOrderDto, byId.get());
            final OrderDto orderDto = mapper.map(order, OrderDto.class);
            return orderDto;
        } else {
            throw new NotFoundException("Account not found");
        }
    }

    /**
     * Returns all orders for given user
     */
    public Page<OrderDto> findAllOrders(GenericSearchDto filter, Pageable pageable, String user) {
        final AccountDto accountByUserName = accountService.findAccountByUserName(user);
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setKey("account");
        searchCriteria.setValue(accountByUserName.getId());
        searchCriteria.setOperation(SearchOperation.IN);
        filter.addCriteria(searchCriteria);
        return orderRepository.findAll(new SearchSpecification<>(filter.getCriteria()), pageable)
                .map(p -> mapper.map(p, OrderDto.class));
    }

    private Order placeOrder(CreateOrderDto createOrderDto, Account account) {
        if (createOrderDto.getType().equalsIgnoreCase("BUY")) {
            return placeBuyOrder(createOrderDto, account);
        } else if (createOrderDto.getType().equalsIgnoreCase("SELL") && hasEnoughShares(createOrderDto, account)) {
            return placeSellOrder(createOrderDto, account);
        } else {
            throw new InvalidStateException(
                    "Bad operation type, should be BUY/SELL, but was:" + createOrderDto.getType());
        }
    }

    private Order placeSellOrder(CreateOrderDto createOrderDto, Account account) {
        throw new RuntimeException("Not imlemented");
    }

    private Order placeBuyOrder(CreateOrderDto createOrderDto, Account account) {
        if (hasEnoughCash(createOrderDto, account)) {
            Order order = mapper.map(createOrderDto, Order.class);
            order.setStatus(OrderStatus.NEW);
            order.setAccount(account);
            orderRepository.save(order);
            account.setCashBalance(account.getCashBalance() - requiredAmountToBuy(createOrderDto));
            accountRepository.save(account);
            return order;
        } else {
            throw new InvalidStateException(
                    "Not enough cash in balance, required" + requiredAmountToBuy(createOrderDto) + " available "
                            + account.getCashBalance());
        }
    }


    private boolean hasEnoughShares(CreateOrderDto createOrderDto, Account account) {
        throw new RuntimeException("Not implemented");
    }

    boolean hasEnoughCash(CreateOrderDto createOrderDto, Account account) {
        return account.getCashBalance() > requiredAmountToBuy(createOrderDto);
    }

    private Double requiredAmountToBuy(CreateOrderDto createOrderDto) {
        return createOrderDto.getMaxPrice() * createOrderDto.getNumberOfShares();
    }

}
