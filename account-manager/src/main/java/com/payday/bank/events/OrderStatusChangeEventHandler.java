package com.payday.bank.events;

import com.payday.bank.service.TransitionService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderStatusChangeEventHandler implements ApplicationListener<OrderStatusChangeEvent> {

    private final TransitionService transitionService;

    @Override
    public void onApplicationEvent(OrderStatusChangeEvent event) {
        System.out.println("Order status change: from"+event.getOldStatus()+"->"+event.getOrderDto().getStatus());

    }
}
