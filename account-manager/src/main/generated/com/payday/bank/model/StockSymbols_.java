package com.payday.bank.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(StockSymbols.class)
public abstract class StockSymbols_ {

	public static volatile SingularAttribute<StockSymbols, String> symbol;
	public static volatile SingularAttribute<StockSymbols, String> name;
	public static volatile SingularAttribute<StockSymbols, String> index;
	public static volatile SingularAttribute<StockSymbols, Long> id;

	public static final String SYMBOL = "symbol";
	public static final String NAME = "name";
	public static final String INDEX = "index";
	public static final String ID = "id";

}

