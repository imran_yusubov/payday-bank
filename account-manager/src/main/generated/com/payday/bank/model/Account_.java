package com.payday.bank.model;

import com.payday.bank.model.user.User;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Account.class)
public abstract class Account_ {

	public static volatile SingularAttribute<Account, Double> totalBalance;
	public static volatile SingularAttribute<Account, Double> cashBalance;
	public static volatile SingularAttribute<Account, Long> id;
	public static volatile SingularAttribute<Account, User> user;

	public static final String TOTAL_BALANCE = "totalBalance";
	public static final String CASH_BALANCE = "cashBalance";
	public static final String ID = "id";
	public static final String USER = "user";

}

